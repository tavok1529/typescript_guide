(() => {
    type Hero = {
        name: string;
        age?: number;
        powers: number[];
        getName?: () => string;
    }

    let myCustomVariables: (string | number | Hero) = 'octavio';
    console.log(typeof myCustomVariables);
    myCustomVariables = 20;
    console.log(typeof myCustomVariables);
    myCustomVariables = {
        name: 'bruce',
        age: 34,
        powers: [1]
    };
    console.log(typeof myCustomVariables);
    console.log(myCustomVariables);

})()