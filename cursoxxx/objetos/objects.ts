(() => {

    // poner ? significa que un parametro es opcional

    let flash: { name: string, age?: number, powers: string[], getName?: () => string } = {
        name: 'Barry allen',
        age: 24,
        powers: ['Super velocidad', 'Viajar en eltiempo']
    }

    let superman: { name: string, age?: number, powers: string[], getName?: () => string } = {
        name: 'Clark kent',
        age: 24,
        powers: ['Super Fuerza']
    }


    // flash = {
    //     name: 'Clark ken',
    //     // age: 23,
    //     powers: ['super fuerza'],
    //     getName() {
    //         return this.name;
    //     }
    // }

    // console.log(flash.getName());


})()