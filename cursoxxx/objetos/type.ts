(() => {

    type Hero = {
        name: string;
        age?: number;
        powers: number[];
        getName?: () => string;

    }

    // poner ? significa que un parametro es opcional

    let flash: Hero = {
        name: 'Barry allen',
        age: 24,
        powers: [1, 2]
    }

    let superman: Hero = {
        name: 'Clark kent',
        age: 60,
        powers: [1],
        getName() {
            return this.name;
        }
    }


})()