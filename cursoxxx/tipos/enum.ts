(() => {
    /*
    el nombre debe iniciar en mayus
    todos lo valores inician en 0 como los array
    
    podemos definir el inicio y el fin 
    */
    enum Volumen {
        min,
        medio,
        max
    };
    enum Volumen2 {
        min = 1,
        medio,
        max = 10
    };

    let minimo: number = Volumen.min;
    let medio: number = Volumen.medio;
    let maximo: number = Volumen.max;

    console.log(minimo);
    console.log(medio);
    console.log(maximo);
    console.log(Volumen);

    let minimo2: number = Volumen2.min;
    let medio2: number = Volumen2.medio;
    let maximo2: number = Volumen2.max;

    console.log(minimo2);
    console.log(medio2);
    console.log(maximo2);
    console.log(Volumen2);

})()
