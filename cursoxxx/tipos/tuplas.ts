(() => {
    /*
    podemos defirnir si los primeros 3 datos son de tipos
    diferentes por ejemplo string, number y boolean
    y en esa posicion se debe tener ese tipo de  dato
    */

    let hero: [string, number, boolean] = ["Dr.Strage ", 100, true];

    //las siguientes asignaciones generan un error
    //hero[0] = 123;
    //hero[1] = "Iron Man";

})();
