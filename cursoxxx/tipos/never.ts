(() => {
    //never es never no es void no es undefined no es null
    // si se rquieres que si se ejecute se le puede indicar que 
    //en el retorno como se muestra a continuacion
    const abc = (message: string = 'Error Occurred..'): (never | number) => {

        if (false) { throw new Error(message); }

        return 1;
    }



    abc('Auxiliio!..')

})();