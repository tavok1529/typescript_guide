(() => {
    let avenger: any = "Superman";
    let existe;
    let derrotas;


    avenger = "Dr. Strange";

    // esto no se puede hacer    
    // console.log(avenger.charAt(0));

    avenger = 150.5000;
    // la siguiente linea genera un error
    console.log(avenger.toFixed(2));

    avenger = true;
    console.log(avenger);

    console.log(existe);
    console.log(derrotas);

    // convertir un any a string
    // mediante un cast
    let edad: any = 123.12321;
    avenger = "helloworl";
    console.log((avenger as string).charAt(0));

    console.log((<number>edad).toFixed(2));
    console.log((edad as number).toFixed(2));

})();

