const a: number = 10;
console.log({ a });
//any es un tipo de dato que acepta lo que sea
let b: any = 3.1415;
console.log("como number " + b);
b = 'hello world';
console.log("como cadena " + b)

function sayhello(msg: string) {
    console.log('Hola ' + msg);
}

sayhello('Ara');